<?php
/**
 * @file
 * Admin page callbacks for the DNS Prefetch module.
 */

/**
 * Form constructor for the DNS Prefetch admin form.
 */
function dns_prefetch_admin() {

  $form['dns_prefetch_protocol_control'] = array(
    '#type' => 'select',
    '#title' => t('DNS prefetch protocol control'),
    '#options' => array(
      DNS_PREFETCH_HTTP_ONLY => t('HTTP Only'),
      DNS_PREFETCH_HTTP_AND_HTTPS => t('HTTP and HTTPS'),
      DNS_PREFETCH_DISABLED => t('Disabled'),
    ),
    '#default_value' => variable_get('dns_prefetch_protocol_control', DNS_PREFETCH_HTTP_ONLY),
    '#description' => t('By default browsers will not use DNS prefetching when a page is served via HTTPS, you must explicitly enable prefetching for HTTPS. Disabling prefetching will prevent browsers using prefetching and any inline attempts to enable it will be ignored.'),
  );

  $form['dns_prefetch_domains'] = array(
    '#title' => t('DNS prefetch domains'),
    '#type' => 'textarea',
    '#description' => t('The domains you wish to be prefetched. Enter a comma separated list of host names. e.g. example.com, another.example.com. New lines will be ignored.'),
    '#default_value' => variable_get('dns_prefetch_domains', ''),
  );

  return system_settings_form($form);
}
